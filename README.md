# `yx` - YAML Data Extraction Tool

A small shell tool that allows extraction of targeted data from YAML.


## Usage
```
$ yx -h
YAML Data Extraction Tool
Usage: yx [<options>] [<path>]
<options> :-
  -h|--help         : show this help
  -d|--doc <n>      : parse <n>th YAML document, default: 1
  -f|--file <file>  : parse YAML from <file>, default: STDIN
  -v|--version      : display version
  --                : end of command options
<path> :-
  <key>|<index> ... : path to the data to extract
<key>               : map key value
<index>             : sequence <index> number, starts with 1
```

If the path you specify does not end at a scalar value, your result will either
be a list of valid mapping keys (one per line)... 
```yaml
# map.yaml
foo: one
bar: two
blah: argh
```
```
$ yx -f map.yaml
foo
bar
blah
```

...or a list of valid sequence indices...
```yaml
# sequence.yaml
- one
- two
- three
```
```
$ yx -f sequence.yaml
1
2
3
```

## Requirements

* C Build Environment (compiler, linker, `make`, etc.)
* [libyaml](https://github.com/yaml/libyaml)

## Installation

### Build the `yx` Binary
```
$ make
```

### Run the Tests
```
$ make test
[...]
*** TESTING RESULTS ***
25 tests, 0 failed.
```

### Install!

The default install `PREFIX` is `/usr`, and the binary installs into `$PREFIX/bin`.
```
$ make install
```
