// vim: et ts=4:

#include <errno.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <yaml.h>


#define VERSION "1.0.2"

const struct option options[] = {
    { "doc",     required_argument, NULL, 'd' },
    { "file",    required_argument, NULL, 'f' },
    { "help",    no_argument,       NULL, 'h' },
    { "version", no_argument,       NULL, 'v' },
    { NULL, 0, NULL, 0 }
};


int usage(int ret) {
    FILE *out = ret == EXIT_SUCCESS ? stdout : stderr;
    fprintf(out, "Usage: yx [<options>] [<path>]\n");
    fprintf(out, "<options> :-\n");
    fprintf(out, "  -h|--help         : show this help\n");
    fprintf(out, "  -d|--doc <n>      : parse <n>th YAML document, default: 1\n");
    fprintf(out, "  -f|--file <file>  : parse YAML from <file>, default: STDIN\n");
    fprintf(out, "  -v|--version      : display version\n");
    fprintf(out, "  --                : end of command options\n");
    fprintf(out, "<path> :-\n");
    fprintf(out, "  <key>|<index> ... : path to the data to extract\n");
    fprintf(out, "<key>               : map key value\n");
    fprintf(out, "<index>             : sequence <index> number, starts with 1\n");
    return ret;
}


int get_event(yaml_parser_t *parser, yaml_event_t *event);
int parse_mapping(yaml_parser_t *parser, int c, char *k[]);
int parse_sequence(yaml_parser_t *parser, int c, char *k[]);
int parse_scalar(yaml_event_t *event, int c, char *k[]);


int main(int argc, char *argv[])
{
    const char *filename = NULL;
    FILE *input = NULL;
    long doc = 0;

    int ch;
    while ((ch = getopt_long(argc, argv, "d:f:hv", options, NULL)) != -1) {
        long lval;
        char *end;

        switch (ch) {
            case 'd':
                errno = 0;
                lval = strtol(optarg, &end, 10);

                if (end == optarg || *end != '\0') {
                    fprintf(stderr, "ERROR: --doc %s: not a number\n", optarg);
                    return usage(EXIT_FAILURE);
                }
                if (lval <= 0) { /* including underflow */
                    fprintf(stderr, "ERROR: --doc %s: %s document number is not valid\n", optarg,
                            lval ? "negative" : "zero");
                    return usage(EXIT_FAILURE);
                }
                if (errno != 0) { /* overflow */
                    fprintf(stderr, "ERROR: --doc %s: number too large\n", optarg);
                    return usage(EXIT_FAILURE);
                }
                if (doc != 0 && doc != lval) {
                    fprintf(stderr, "ERROR: multiple conflicting --doc options\n");
                    return usage(EXIT_FAILURE);
                }

                doc = lval;
                break;

            case 'f':
                if (filename != NULL && strcmp(filename, optarg) != 0) {
                    fprintf(stderr, "ERROR: multiple conflicting --file options\n");
                    return usage(EXIT_FAILURE);
                }

                filename = optarg;
                break;

            case 'h':
                return usage(EXIT_SUCCESS);

            case 'v':
                printf("yx v%s\n", VERSION);
                return EXIT_SUCCESS;

            case '?':           /* FALLTHROUGH */
            default:
                /* unknown option */
                return usage(EXIT_FAILURE);
        }
    }

    if (filename == NULL || strcmp(filename, "-") == 0) {
        input = stdin;
        filename = "(stdin)"; /* if we'll need it for error messages */
    }
    else {
        input = fopen(filename, "rb");
        if (input == NULL) {
            perror(filename);
            return usage(EXIT_FAILURE);
        }
    }

    /* if --doc is not specifed, use the first document */
    if (doc == 0) {
        doc = 1;
    }

    /* skip to keys/indices */
    argc -= optind;
    argv += optind;

    // Initialize YAML Parser

    int ret = 0;
    yaml_parser_t parser;
    yaml_event_t event;

    if (!yaml_parser_initialize(&parser)) {
        fprintf(stderr, "ERROR: could not initialize YAML parser\n");
        return 1;
    }
    yaml_parser_set_input_file(&parser, input);

    // Main Parsing Loop

    while (1) {
        yaml_event_type_t type;

        if (!get_event(&parser, &event)) {
            ret = 1;
            break;
        }
        type = event.type;

        if (type == YAML_DOCUMENT_START_EVENT)
            doc--;
        else if (!doc) {
            if (type == YAML_MAPPING_START_EVENT) {
                yaml_event_delete(&event);
                ret = parse_mapping(&parser, argc, argv);
                break;
            }
            else if (type == YAML_SEQUENCE_START_EVENT) {
                yaml_event_delete(&event);
                ret = parse_sequence(&parser, argc, argv);
                break;
            }
            else if (type == YAML_SCALAR_EVENT) {
                ret = parse_scalar(&event, argc, argv);
                break;
            }
            else if (type == YAML_DOCUMENT_END_EVENT) {
                fprintf(stderr, "ERROR: key-path not found in YAML\n");
                ret = 1;
                break;
            }
        }
        else if (type == YAML_STREAM_END_EVENT)
            break;

        yaml_event_delete(&event);
    }
    if (!ret && doc) {
        fprintf(stderr, "ERROR: requested document not found\n");
        ret = 1;
    }

    /* cleanup */
    fclose(input);
    yaml_parser_delete(&parser);
    fflush(stdout);

    return ret;
}


int get_event(yaml_parser_t *parser, yaml_event_t *event) {
    if (!yaml_parser_parse(parser, event)) {
        if (parser->problem_mark.line || parser->problem_mark.column) {
            fprintf(stderr, "ERROR: YAML parsing (%lu,%lu): %s\n",
                (unsigned long)parser->problem_mark.line + 1,
                (unsigned long)parser->problem_mark.column + 1,
                parser->problem);
            }
        else 
            fprintf(stderr, "ERROR: YAML parsing: %s\n", parser->problem);
        return 0;
    }
    return 1;
}


int parse_mapping(yaml_parser_t *parser, int c, char *k[]) {
    yaml_event_t event;
    yaml_event_type_t type;
    int depth = 0;
    char *key = NULL;
    
    if (!get_event(parser, &event))
        return 1;
    type = event.type;

    while (depth || type != YAML_MAPPING_END_EVENT) {
        // sort out key
        if (!depth) {
            if (!key) {
                if (type == YAML_SCALAR_EVENT) {
                    key = malloc(event.data.scalar.length + 1);
                    strncpy(key, event.data.scalar.value, event.data.scalar.length + 1);
                    yaml_event_delete(&event);
                    if (!get_event(parser, &event))
                        return 1;
                    type = event.type;
                }
                else if (type == YAML_MAPPING_START_EVENT || type == YAML_SEQUENCE_START_EVENT) {
                    fprintf(stderr, "ERROR: unsupported non-scalar map key detected\n");
                    return 1;
                }
            }
        }
        if (!c) {
            // no more keys, but we're not at a scalar, output keys
            switch (type) {
                case YAML_MAPPING_START_EVENT:
                case YAML_SEQUENCE_START_EVENT:
                case YAML_SCALAR_EVENT:
                    if (!depth) {
                        printf("%s\n", key);
                        free(key);
                        key = NULL;
                    }
                    if (type != YAML_SCALAR_EVENT)
                        depth++;
                    break;
                case YAML_MAPPING_END_EVENT:
                case YAML_SEQUENCE_END_EVENT:
                    depth--;
            }
        }
        else {    
            switch (type) {
                case YAML_MAPPING_START_EVENT:
                case YAML_SEQUENCE_START_EVENT:
                case YAML_SCALAR_EVENT:
                    if (!depth) {
                        if (!strcmp(k[0], key)) {
                            // this is the key we're looking for
                            free(key);
                            key = NULL;
                            if (type == YAML_MAPPING_START_EVENT) {
                                yaml_event_delete(&event);
                                return parse_mapping(parser, c - 1, &k[1]);
                            }
                            else if (type == YAML_SEQUENCE_START_EVENT) {
                                yaml_event_delete(&event);
                                return parse_sequence(parser, c - 1, &k[1]);
                            } else {
                                return parse_scalar(&event, c - 1, &k[1]);
                            }
                        }
                        else {
                            free(key);
                            key = NULL;
                        }
                    }
                    if (type != YAML_SCALAR_EVENT)
                        depth++;
                    break;
               case YAML_MAPPING_END_EVENT:
               case YAML_SEQUENCE_END_EVENT:
                    depth--;
            }
        }
        yaml_event_delete(&event);
        if (!get_event(parser, &event))
            return 1;
        type = event.type;
    }
    if (c) {
        fprintf(stderr, "ERROR: unable to find map key '%s'\n", k[0]);
        return 1;
    }
    return 0;
}


int parse_sequence(yaml_parser_t *parser, int c, char *k[]) {
    yaml_event_t event;
    yaml_event_type_t type;
    int depth = 0;
    int n = 0;
    
    if (c) {
        n = atoi(k[0]);
        if (!n) {
            fprintf(stderr, "ERROR: sequence key '%s' not positive integer\n", k[0]);
            return 1;
        }
    }
    if (!get_event(parser, &event))
        return 1;
    type = event.type;

    while (depth || type != YAML_SEQUENCE_END_EVENT) {
        if (!c) {
            // no more keys, but we're not at a scalar, output indexes
            switch (type) {
                case YAML_MAPPING_START_EVENT:
                case YAML_SEQUENCE_START_EVENT:
                case YAML_SCALAR_EVENT:
                    if (!depth)
                        printf("%d\n", ++n);
                    if (type != YAML_SCALAR_EVENT)
                        depth++;
                    break;
                case YAML_MAPPING_END_EVENT:
                case YAML_SEQUENCE_END_EVENT:
                    depth--;
            }
        }
        else {    
            switch (type) {
                case YAML_MAPPING_START_EVENT:
                case YAML_SEQUENCE_START_EVENT:
                case YAML_SCALAR_EVENT:
                    if (!depth) {
                        if (!--n) {
                            // this is the index we're looking for
                            if (type == YAML_MAPPING_START_EVENT) {
                                yaml_event_delete(&event);
                                return parse_mapping(parser, c - 1, &k[1]);
                            }
                            else if (type == YAML_SEQUENCE_START_EVENT) {
                                yaml_event_delete(&event);
                                return parse_sequence(parser, c - 1, &k[1]);
                            } else {
                                return parse_scalar(&event, c - 1, &k[1]);
                            }
                        }
                    }
                    if (type != YAML_SCALAR_EVENT)
                        depth++;
                    break;
               case YAML_MAPPING_END_EVENT:
               case YAML_SEQUENCE_END_EVENT:
                    depth--;
            }
        }
        yaml_event_delete(&event);
        if (!get_event(parser, &event))
            return 1;
        type = event.type;
    }
    if (c) {
        fprintf(stderr, "ERROR: unable to find sequence key '%s'\n", k[0]);
        return 1;
    }
    return 0;
}


int parse_scalar(yaml_event_t *event, int c, char *k[]) {
    if (c != 0) {
        fprintf(stderr, "ERROR: scalar found where key '%s' expected\n", k[0]);
        return 1;
    }
    // TODO: we may want to optionally base64 encode some data?
    // value may contain NUL chars, output each char separately
    for (size_t i = 0; i < event->data.scalar.length; i++) {
        putchar(event->data.scalar.value[i]);
    }
    return 0;
}

