#!/bin/sh
# vim: et ts=4:

: "${VERBOSE:=0}"
[ "$1" = "-v" ] && VERBOSE=1

TESTS=0
FAILS=0

# shell strips all final newlines in the command substitution, so
# emit a final sentinel and then strip it afterwards in the caller
#
#   out="$(with_sentinel printf 'a\n')
#   out="${out%|}"   # strip sentinel
#   # out is set to 'a\n'
#
with_sentinel() {
    local retval
    "$@"
    retval=$?
    printf '|'		       # sentinel to protect trailing newlines
    return $retval
}


#
# assert "Descriptive Test Name"  \
#     "../yx -foo -some file"     \
#     [ succeed | fail | error ]  \
#     [output ...]
#
# The output is used as arguments to printf(1) to construct the
# intended final result so that the caller doesn't have to do that
# manually each time it needs a newline or something.  If you need to
# use any characters that printf interprets just use '%s' format and
# supply the literal string as the next argument, e.g., '%s' '%s\n' to
# specify the literal '%s\n'.
#
assert() {
    local testing="$1"
    local execute="$2"
    local should="$3"
    shift 3

    local expected="${1+$(with_sentinel printf "$@")}"
    expected="${expected%|}"

    local result
    local retval

    TESTS=$((TESTS+1))
    printf "%s %s..." --- "$testing"

    result="$(with_sentinel $execute 2>&1)"
    retval=$?
    result="${result%|}"

    if [ $retval -ne 0 ]; then
        if [ "$should" = "error" ]; then
            printf "error_ok (%d)\n" "$retval"
        else
            FAILS=$((FAILS+1))
            printf "ERROR (%d)\n" "$retval"
        fi
    elif [ "$result" != "$expected" ]; then
        if [ "$should" = "fail" ]; then
            printf "fail_ok\n"
        else
            FAILS=$((FAILS+1))
            printf "FAIL\n"
        fi
    else
        if [ "$should" = "succeed" ]; then
            printf "success\n"
        else
            FAILS=$((FAILS+1))
            printf "SUCCESS_!OK\n"
        fi
    fi
    [ "$VERBOSE" -eq 1 ] && printf "%s\n\n" "$result"
}

printf "\n=== INPUT ===\n\n"

echo stdin |
assert "From Implicit STDIN" \
    "../yx" \
    "succeed" \
    "stdin"

echo stdin |
assert "From Explicit STDIN" \
    "../yx -f -" \
    "succeed" \
    "stdin"

assert "From Non-Existent File" \
    "../yx -f does-not-exist.yaml" \
    "error"

assert "From Invalid YAML File" \
    "../yx -f ../yx" \
    "error"

printf "\n=== DOCUMENT SELECTION ===\n\n"

assert "Implicit Document 1" \
    "../yx -f test__documents.yaml" \
    "succeed" \
    "one"

assert "Explicit Document 1" \
    "../yx -d 1 -f test__documents.yaml" \
    "succeed" \
    "one"

assert "Explicit Document 2" \
    "../yx -d 2 -f test__documents.yaml" \
    "succeed" \
    "two"

assert "Invalid Document 3" \
    "../yx -d 3 -f test__documents.yaml" \
    "error"

assert "Invalid Document 0" \
    "../yx -d 0 -f test__documents.yaml" \
    "error"

assert "Invalid Document 'foo'" \
    "../yx -d foo -f test__documents.yaml" \
    "error"

printf "\n=== SCALARS ===\n\n"

assert "Flow Plain" \
    "../yx -f test__scalar-flow-plain.yaml" \
    "succeed" \
    "plain\nflow"

assert "Flow Single Quotes" \
    "../yx -f test__scalar-flow-single.yaml" \
    "succeed" \
    "single 'quoted'\nflow"

assert "Flow Double Quotes" \
    "../yx -f test__scalar-flow-double.yaml" \
    "succeed" \
    "double \"quoted\"\nflow"

assert "Block Folded (clip)" \
    "../yx -f test__scalar-block-folded-clip.yaml" \
    "succeed" \
    "block folded\nclip trailing\n"

assert "Block Literal (clip)" \
    "../yx -f test__scalar-block-literal-clip.yaml" \
    "succeed" \
    "block literal\n\nclip trailing\n"

assert "Block Literal (keep)" \
    "../yx -f test__scalar-block-literal-keep.yaml" \
    "succeed" \
    "block literal\n\nkeep trailing\n\n\n"

assert "Block Literal (strip)" \
    "../yx -f test__scalar-block-literal-strip.yaml" \
    "succeed" \
    "block literal\n\nstrip trailing"



printf "\n=== MAPPINGS ===\n\n"

assert "No Key Specified Returns All Keys" \
    "../yx -f test__mappings.yaml" \
    "succeed" \
    "foo\nbar\nblah\n"

assert "Key 'foo' to Scalar" \
    "../yx -f test__mappings.yaml foo" \
    "succeed" \
    "one"

assert "Nested: Three 'blah' Keys to Scalar" \
    "../yx -f test__mappings.yaml blah blah blah" \
    "succeed" \
    "three"

assert "Unknown Key 'wtf'" \
    "../yx -f test__mappings.yaml wtf" \
    "error"

printf "\n=== SEQUENCES ===\n\n"

assert "No Index Returns All Indicies" \
    "../yx -f test__sequences.yaml" \
    "succeed" \
    "1\n2\n3\n"

assert "Invalid Index 0" \
    "../yx -f test__sequences.yaml 0" \
    "error"

assert "Index 1 to Scalar" \
    "../yx -f test__sequences.yaml 1" \
    "succeed" \
    "one"

assert "Nested: Three Indices (3 2 1) to Scalar" \
    "../yx -f test__sequences.yaml 3 2 1" \
    "succeed" \
    "three Two ONE"

assert "Unknown Index 4" \
    "../yx -f test__sequences.yaml 4" \
    "error"

assert "Invalid Index 'foo'" \
    "../yx -f test__sequences.yaml foo" \
    "error"

printf "\n=== MAPPING / SEQUENCE COMBINATIONS ===\n\n"

assert "Map -> Sequence -> Map -> Sequence to Scalar" \
    "../yx -f test__map-seq-map-seq.yaml bar 2 argh 2" \
    "succeed" \
    "five"

assert "Sequence -> Map -> Sequence -> Map to Scalar" \
    "../yx -f test__seq-map-seq-map.yaml 2 bar 2 blah" \
    "succeed" \
    "five"

printf "\n*** TESTING RESULTS ***\n"
printf "%d tests, %d failed.\n\n" $TESTS $FAILS
exit $FAILS
